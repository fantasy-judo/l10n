## Global

it = italiano

athlete                       = Athlete
become_patron                 = BECOME A PATRON
email                         = Email
events                        = Events
ijf_profile                   = IJF Profile
ippon                         = Ippon
info                          = Info
international_judo_federation = International Judo Federation
log-in                        = Log In
log-out                       = Log Out
password                      = Password
picks                         = Picks
points                        = Points 
results                       = Results
shido                         = Shido
team                          = Team
wazari                        = Wazari

multi_lingual_1 = Multilingual Fantasy Judo is very much work in progress.
multi_lingual_2 = Think you can help with translations? Please
multi_lingual_3 = email us

## Index

index_hero_text     = Play Fantasy Judo against other Judo fans
index_hero_email    = Email
index_hero_password = Password
index_hero_sign_up  = Sign Up

index_intro_text = Select your Judo athletes for each event to create your strongest team. Choose carefully from athletes on the world ranking list and spend your budget wisely. Top players will cost you more than lower ranked ones.

index_create_team_h2 = Create a Team
index_create_team_1  = Your team name is unique, but if you want a change you can change it to any available name at any time. Your results stay with you.
index_create_team_2  = Your emblem is your team badge; customise the shapes and colours and make it your own!

index_pick_athletes_h2 = Pick Your Athletes
index_pick_athletes_1  = For each event, pick as many or as few athletes as you like. Your budget is limited though so you can't just pick top athletes.
index_pick_athletes_2  = Select a captain and they'll earn double points; pick carefully though as shidos also score double!

index_climb_leaderboard_h2 = Climb the Leaderboard
index_climb_leaderboard_1  = Compare how your team does against other teams, see if you can get the top spot…or at least get higher than the other people in your dojo.
index_climb_leaderboard_2  = Choosing early and updating after the draw are good ways to gain some extra points.

index_fine_tune_h2 = Fine Tune Your Picks
index_fine_tune_1  = Don't rest on your laurels—picking early could score you a bargain but as inscriptions change, especially after the draw, your picks might need tweaking.
index_fine_tune_2  = With every event in the IJF World Tour you won't be short of things to do!

index_follow_action_h2 = Follow the Action
index_follow_action_1  = For IJF world tour events your team score will be updated regularly during the competition, so you can follow how your team does.
index_follow_action_2  = For smaller events we update on Monday, when the results are made available.

index_discuss_h2 = Discuss All the Fun
index_discuss_1  = As well as talking about your team's results at the dojo, chat with us and others on { $discordLink }, { $facebookLink }, and { $twitterLink }.

index_free_h2 = Play for Free
index_free_1  = This site is entirely free to play, with no adverts. We are entirely supported by our patrons on { $patreonLink }.
index_free_2  = So please consider making a small contribution each month to help keep the site online and improving.

## Events

events_7am_local_time = (7am local time)
events_closes_in = Closes in
events_opens_in = Opens in
events_remaining = remaining

## Forgot Password

forgot_password = Forgot password
forgot_password_sent_1 = Password reset email on it's way
forgot_password_sent_2 = Please check your inbox for an email from no-reply@fantasy-judo.com.
forgot_password_sent_3 = The email will contain a link to a page where you can set a new password.

## Log-in

login_alert_reset_success = Password reset successfully, please log in.
login_alert_reset_failure = Incorrect email or password, please try again.

## Picks

picks_back    = Back
picks_save    = Save
picks_alert   = Pick a captain to score double points with that athlete; pick carefully though as shidos also score double!
picks_caption = *Statistics cover events from January 2019 to date.
picks_cost    = Cost
picks_wins    = Wins

## Privacy

privacy_our_contact_details     = Our contact details
privacy_data_protection_officer = Data Protection Officer
privacy_what_info               = What type of information we have
privacy_what_info_2             = We currently collect and process the following information:
privacy_what_info_3             = Email address
privacy_how_and_why             = How we get the information and why we have it
privacy_how_and_why_2           = Most of the personal information we process is provided to us directly by you for one of the following reasons:
privacy_how_and_why_3           = Account creation
privacy_how_and_why_4           = Email contact (via opt-in on your settings page, consent can be revoked at anytime via this page also)

privacy_gdpr_header             = Under the General Data Protection Regulation (GDPR), the lawful bases we rely on for processing this information are:
privacy_gdpr_1                  = Your consent. You are able to remove your consent at any time.
privacy_gdpr_2                  = We have a contractual obligation.
privacy_gdpr_3                  = We have a legal obligation.
privacy_gdpr_4                  = We have a vital interest.
privacy_gdpr_5                  = We need it to perform a public task.
privacy_gdpr_6                  = We have a legitimate interest.

privacy_what_info_header        = What we do with the information we have
privacy_what_info_1             = We use the information that you have given us in order to provide a method to contact users to update them on site news and site updates.
privacy_what_info_2             = We may share this information with no third party organisations, exccept email service providers when sending emails.

privacy_how_we_store_header     = How we store your information
privacy_how_we_store_1          = Your information is securely stored London, UK.
privacy_how_we_store_2          = We keep your email while your account exists. We will then dispose your information by removing your email from our database.

privacy_your_rights             = Your data protection rights
privacy_your_rights_2           = Under data protection law, you have rights including:
privacy_your_rights_3           = Your right of access - You have the right to ask us for copies of your personal information.
privacy_your_rights_4           = Your right to rectification - You have the right to ask us to rectify information you think is inaccurate. You also have the right to ask us to complete information you think is incomplete.
privacy_your_rights_5           = Your right to erasure - You have the right to ask us to erase your personal information in certain circumstances.
privacy_your_rights_6           = Your right to restriction of processing - You have the right to ask us to restrict the processing of your information in certain circumstances.
privacy_your_rights_7           = Your right to object to processing - You have the the right to object to the processing of your personal data in certain circumstances.
privacy_your_rights_8           = Your right to data portability - You have the right to ask that we transfer the information you gave us to another organisation, or to you, in certain circumstances.
privacy_your_rights_9           = You are not required to pay any charge for exercising your rights. If you make a request, we have one month to respond to you.

privacy_complain                = How to complain
privacy_complain_2              = You can also complain to the ICO if you are unhappy with how we have used your data.

## Results

results_most_picked = Most Picked
results_top_scoring = Top Scoring

results_team_capt         = Capt.
results_team_collapse_all = Collapse All
results_team_opponent     = Opponent
results_team_round        = Round
results_team_total        = Total
results_team_versus_me    = Versus Me
results_team_win          = Win

results_compare_category  = Category

## Info

info_about_intro_1  = This page shares a little about the Fantasy Judo website, the tools we use and the people who have built it.

info_about_who      = Who
info_about_who_2    = Fantasy Judo has been created by 
info_about_who_3    = and 
info_about_who_4    = Work colleagues, James is the technical mind responsible for a majority of the technical excellence the site strives for. Lance is a Judo coach and member of the European Judo Union and International Judo Federation IT teams so adds the Judo specific knowledge.

info_about_what     = What
info_about_what_2   = The site has been written in 
info_about_what_3   =  with a 
info_about_what_4   =  database. The data and photos come directly from the 
info_about_what_5   =  data systems. The icons are from 
info_about_what_6   =  and images used in emblem creation from 

info_about_where    = Where
info_about_where_2  = The site is hosted on 
info_about_where_3  = , the code is stored on 
info_about_where_4  = . The domain is register and managed on 
info_about_where_5  =  and James and Lance are based in the South of England in the UK.

info_about_when    = When
info_about_when_2  = The development started in December 2018, first release was to a test group of users on January 1st 2019. The first event was the Tel Aviv Grand Prix on 24th January when the results system was used on the site for the first time.

info_about_why     = Why
info_about_why_2   = Within the Judo community at least three versions of a Fantasy Judo system had existed but died; with the last being run in 2017 and the earliest being run by Mathias Fischer on 
info_about_why_3   =  There had been many discussions by Judo IT people about the idea of a Fantasy Judo project over many years but it had never bubbled to the top of peoples TODO lists. In December 2018 James and Lance spoke about the idea over beers afterwork and decided that it would be a fun side project to work on and set to work. James previously had experience with Go for the web via his successful 
info_about_why_4   = website so Go was decided upon as the language for the site with James building vast swathes of the code while Lance provided the Judo specific knowledge.
