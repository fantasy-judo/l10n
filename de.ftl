## Global

de = Deutsch

athlete                       = Athlet
become_patron                 = Werde ein Patron (Werde Mitglied bei Patron)
email                         = Email
events                        = Events
ijf_profile                   = IJF Profil
ippon                         = Ippon
info                          = Info
international_judo_federation = International Judo Federation
log-in                        = Einloggen
log-out                       = Ausloggen
password                      = Passwort
picks                         = Auswahl
points                        = Punkte
results                       = Ergebnisse
shido                         = Shido
team                          = Team
wazari                        = Wazari

multi_lingual_1 = Die mehrsprachige Version von Fantasy Judo ist in Arbeit.
multi_lingual_2 = Denkst Du, dass Du uns damit helfen kannst?
multi_lingual_3 = Dann schreib uns gerne eine Email.

## Index

index_hero_text     = Spiele Fantasy Judo gegen andere Judofans
index_hero_email    = Email
index_hero_password = Passwort
index_hero_sign_up  = Anmelden

index_intro_text = Wähle Deine Athleten für jedes Event, um Deine stärkste Mannschaft zusammenzustellen. Wähle weise aus allen Athleten von der Weltrangliste und gibt dein Budget sorgfältig aus. Top Kämpfer kosten mehr Punkte, als Kämpfer mit einem niedrigen Listenplatz.

index_create_team_h2 = Erstelle ein Team
index_create_team_1  = Dein Teamname ist einzigartig, aber wenn Du ihn ändern möchtest, dann kannst Du ihn jeder Zeit zu einem anderen verfügbaren Namen ändern. Deine Ergebnisse bleiben dabei erhalten.
index_create_team_2  = Dein Emblem ist das Team-Wappen. Stelle dir die Farben und Formen zusammen und mache es so zu Deinem Wappen.

index_pick_athletes_h2 = Wähle einen Athleten
index_pick_athletes_1  = Du kannst bei jedem Event so viele oder so wenige Athleten wählen wie du möchtest. Dein Budget ist begrenzt, damit Du nicht nur kannst Top-Kämpfer auswählen kannst.
index_pick_athletes_2  = Wähle einen Team-Kapitän. Er oder sie wird Dir doppelte Punkte bringen; wähle weise, denn auch Shidos zählen doppelt!

index_climb_leaderboard_h2 = Steige in der Rangliste auf
index_climb_leaderboard_1  = Vergleiche, wie sich Dein Team gegen andere schlägt, ob Du die Rangliste anführst, ... oder zumindest besser als Deine Freunde aus Deinem Dojo bist.
index_climb_leaderboard_2  = Früh zu wählen und nach dem Auslosen Änderungen vorzunehmen, sind gute Möglichkeiten, um einige Extrapunkte zu sammeln.

index_fine_tune_h2 = Verfeinere or Verbessere Deine Auswahlen
index_fine_tune_1  = Ruhe dich nicht auf deinen Lorbeeren aus - frühes Wählen kann Dir ein Schnäppchen einbringen aber wenn die Einschreibung sich ändert, besonders nach dem Losen, könnte deine Auswahl eine Anpassung benötigen
index_fine_tune_2  = Bei jedem Event der IJF World Tour wird es immer etwas für Dich zu tun geben.

index_follow_action_h2 = Verfolge das Geschehen
index_follow_action_1  = Während der IJF World Tour wird der Punktestand Deines Teams regelmäßig aktualisiert, sodass Du die Leistungen Deines Teams verfolgen kannst.
index_follow_action_2  = Bei kleineren Events werden wir den Punktestand aktualisieren, sobald uns die Ergebnisse zur Verfügung stehen.

index_discuss_h2 = Kommentiere und Diskutiere
index_discuss_1  = Genau so, wie Du in Deinem Dojo über die Leistungen Deines Teams sprechen kannst, kannst Du mit uns und anderen auf (insert Discord), (instert Facebook) und (insert Twitter) diskutieren, chatten und Dich austauschen.

index_free_h2 = Spiele kostenlos
index_free_1  = Diese Seite ist komplett kostenlos und ohne Werbung. Wir finanzieren uns komplett durch unsere Unterstützer, unsere Patrons, auf (instert Patreon).
index_free_2  = Also überlege Dir bitte, ob Du uns monatlich mit einen kleinen Betrag unterstützen möchtest, um die Seite stetig verbessern und online halten zu können.

## Events

events_7am_local_time = (7 Uhr morgens, Ortszeit)
events_closes_in = schließt in
events_opens_in = öffnet in
events_remaining = verbleibend

## Passwort vergessen

forgot_password = Passwort vergessen
forgot_password_sent_1 = Email zum Zurücksetzen des Passwortes ist unterwegs
forgot_password_sent_2 = Bitte schaue in Deinem Posteingang nach: no-reply@fantasy-judo.com.
forgot_password_sent_3 = Die Email enthält einen Link, der Dich zu einer Seite zum Zurücksetzen Deines Passwortes führt.

## Log-in

login_alert_reset_success = Passwort erfolgreich zurückgesetzt, bitte logge Dich ein.
login_alert_reset_failure = Falsche Email-Adresse oder falsches Passwort. Probiere es bitte erneut.
## Picks

picks_back    = Zurück
picks_save    = Speichern
picks_alert   = Wähle einen Mannschaftskapitän aus; dieser wird Did doppelte Punkte bringen. Wähle weise, denn auch Shidos zählen doppelt!
picks_caption = *Statistics cover events from January 2019 to date.
picks_cost    = Kosten
picks_wins    = Gewinn

## Privatsphäre

privacy_our_contact_details     = Unsere Kontaktdaten
privacy_data_protection_officer = Lance Wicks Datenschutzbeauftragter (insert number and mail here)
privacy_what_info               = Welche Art von Informationen wir besitzen
privacy_what_info_2             = Momentan sammeln und verarbeiten wir folgende Informationen:
privacy_what_info_3             = Email Adresse
privacy_how_and_why             = Wie wir an diese Informationen gelangen und warum wir sie benutzen
privacy_how_and_why_2           = Deine privaten Informationen werden uns von Dir selbst zur Verfügung gestellt. Wir brauchen sie aus folgenden Gründen:
privacy_how_and_why_3           = Zum Erstellen Deines Kontos
privacy_how_and_why_4           = Email-Kontakt (via opt-in in den Einstellungen; dies kann dort auch widerrufen werden)

privacy_gdpr_header             = Nach der Datenschutz-Grundverordnung (DSGVO) stützen wir uns bei der Verarbeitung dieser Informationen auf folgende Rechtsgrundlagen:
privacy_gdpr_1                  = Deine Zustimmung; Du kannst sie jeder Zeit widerrufen
privacy_gdpr_2                  = Wir haben eine Vertragspflicht.
privacy_gdpr_3                  = Wir haben eine gesetzliche Verpflichtung.
privacy_gdpr_4                  = Wir haben ein vitales Interesse.
privacy_gdpr_5                  = Wir brauchen es, um öffentliche ? .
privacy_gdpr_6                  = Wir haben ein berechtigtes Interesse.

privacy_what_info_header        = Wie verarbeiten wir die Informationen, die wir besitzen?
privacy_what_info_1             = Wir verwenden die Informationen, die Du uns gegeben hast, um eine Methode zur Kontaktaufnahme mit Nutzern zur Verfügung zu stellen, um Dich über Neuigkeiten und Updates der Website zu informieren.
privacy_what_info_2             = Die Daten werden nicht an Dritte weitergegeben; außer an die Email-Provider.

privacy_how_we_store_header     = Wie wir Deine Informationen speichern
privacy_how_we_store_1          = Your information is securely stored London, UK.
privacy_how_we_store_2          = We keep your email while your account exists. We will then dispose your information by removing your email from our database.

privacy_your_rights             = Deine Datenschutzrechte
privacy_your_rights_2           = Nach den Datenschutzrichtlinien, hast Du das Recht:
privacy_your_rights_3           = Recht auf auskunft: Du hast das Recht, eine Kopie, Deiner von uns gespeicherten Daten anzufordern
privacy_your_rights_4           = Recht auf Berichtigung: Du hast das Recht zu verlangen, dass Informationen, die Du als falsch ansiehst, berichtigen zu lassen; außerdem hast Du das Recht, unvollständige Daten vervollständigen zu lassen.
privacy_your_rights_5           = Recht auf Löschung: Du hast das Recht, Deine personenbezogenen Daten unter bestimmten Umständen von uns löschne zu lassen.
privacy_your_rights_6           = Recht auf Einschränkung der Verarbeitung: Du hast das Recht, unter Umständen, eine Einschränkung der Verarbeitung Deiner Daten von uns zu verlangen.
privacy_your_rights_7           = Recht auf Widerspruch: Du hast das Recht, der Verarbeitung Deiner Daten unter bestimmten Umständen zu widersprechen.
privacy_your_rights_8           = Recht auf Datenübertragung: Du hast das Recht von uns zu verlangen, dass wir unter bestimmten Umständen Deine Daten an Dritte weiterleiten.
privacy_your_rights_9           = Du bist nicht verpflichtet, Gebühren für die Ausübung Deiner Rechte zu zahlen. Wenn Du eine Anfrage stellst, haben wir einen Monat Zeit, um Dir zu antworten.

privacy_complain                = Wie man Beschwerde einreicht
privacy_complain_2              = Du kannst Dich auch beim ICO beschweren, wenn es Dir nicht gefällt, wie wir mit Deinen Daten umgehen.

## Ergebnisse

results_most_picked = Am meisten gewählt
results_top_scoring = Die meisten Punkte

results_team_capt         = Kapitän
results_team_collapse_all = Alle ausblenden
results_team_opponent     = Gegner
results_team_round        = Runde
results_team_total        = Gesamt
results_team_versus_me    = Gegen mich
results_team_win          = Gewinn

results_compare_category  = Kategorie

## Info

info_about_intro_1  = Diese Seite erzählt euch ein wenig über die Fantasy Judo Website, den technischen Aspekten auf der die Seite aufgebaut ist und die Entwickler dieser Seite.

info_about_who      = Wer (steckt dahinter? sounds better)
info_about_who_2    = Fantasy Judo has wurde von
info_about_who_3    = (insert James Raspass) und (insert Lance Wicks here) entwickelt.
info_about_who_4    = James und Lance sind Arbeitskollegen; während James, das technischer Genie, für den Großteil der exzellenten Programmierung der Seite zuständig ist, kümmert sich Lance, als Judotrainer und Mitglied des EJU and IJF IT-Teams, um das judospezifische Wissen.


info_about_what     = Was
info_about_what_2   = Die Seite ist in (insert Go here) geschrieben
info_about_what_3   =  mit einer (insert PostgreSQL here)
info_about_what_4   =  Database. Die Daten und Fotos kommen direkt von der (insert IJF here)
info_about_what_5   =  Die Icons sind von (insert Font Awesome here)
info_about_what_6   =  und die Bilder, die für die Wappen genutzt werden, sind von (insert Freepik here)

info_about_where    = Wo
info_about_where_2  = Die Seite ist auf (insert Digital Ocean here) gehostet
info_about_where_3  = , der Code ist auf (insert GitLab here) gespeichert
info_about_where_4  = . Die Domain ist auf (insert here) registriert und wird in (insert Cloudflare here) gemanaged.
info_about_where_5  =  James und Lance kommen aus dem Süden von dem Vereinigten Königreich.

info_about_when    = Wann (ist die Idee entstanden? sounds better)
info_about_when_2  = Die Entwicklung startete in Dezember 2018; die erste Veröffentlichung für eine Testgruppe von Erstbenutzern war am 01.01.2019. Das aller erste Event, an dem das Resultat-System dieser Seite benutzt wurde, war der Tel Aviv Grand Prix am 24.01.2019.


info_about_why     = Warum (Fantasy Judo? Sounds better)
info_about_why_2   = In der Vergangenheit gab es bereits mindestens drei Versionen von Fantasy Judo, keine davon setzte sich durch; die letzte 2017, die erste auf (insert Ippon.org here) von Mathias Fischer.
info_about_why_3   = Es gab einige Diskussionen von IT-Entwicklern über ein Fantasy Judo Projekt, aber keine der Ideen hat es jemals nach oben auf den Todo-Listen geschafft. Im Dezember 2018 haben James und Lance bei einem Feierabendbier über diese Idee gesprochen und sich dazu entschieden, dass es ein witziges Nebenbeiprojekt sein könnte, wenn sie es umsetzen und zum Laufen bringen. Da James bereits mit seiner erfolgreichen Webiste (insert Code Golfe here) Erfahrung mit Go gesammelt;
info_about_why_4   = so wurde entschieden, dass Go als Sprache für die Seite benutzt werden würde. James hat das Grundgerüst der Webiste erstellt, während Lance es mit seinem judospezifischen Wissen gefüllt hat.
