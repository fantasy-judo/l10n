﻿## Global


fr = Français


athlete                       = Athlète
become_patron                 = DEVENIR UN PARRAIN
email                         = Email
events                        = Épreuves
ijf_profile                   = Profil IJF
ippon                         = Ippon
info                          = Info
international_judo_federation = Fédération Internationale de Judo (IJF)
log-in                        = Connexion
log-out                       = Déconnexion
password                      = Mot de passe
picks                         = Sélections
points                        = Points
results                       = Résultats
shido                         = Shido
team                          = Équipe
wazari                        = Wazari


multi_lingual_1 = La version multilingue de Fantasy Judo est en progrès.
multi_lingual_2 = Vous pensez pouvoir aider avec la traduction? SVP
multi_lingual_3 = écrivez-nous


## Index


index_hero_text     = Jouez à Fantasy Judo contre d’autres fans de judo
index_hero_email    = Email
index_hero_password = Mot de passe
index_hero_sign_up  = S’inscrire


index_intro_text = Sélectionnez vos athlètes de judo pour chaque épreuve afin de créer votre équipe la plus forte. Faites votre choix parmi les athlètes figurant sur la liste du classement mondial et dépensez votre budget à bon escient. Les meilleurs joueurs vous coûteront plus cher que les moins bien classés.


index_create_team_h2 = Créer une équipe
index_create_team_1  = Votre nom d’équipe est unique, mais si vous désirez le modifier, vous pouvez à tout moment le changer pour n’importe quel autre nom disponible. Vos résultats seront conservés.
index_create_team_2  = Votre emblème est votre badge d’équipe; personnalisez les formes et les couleurs pour vous l’approprier!


index_pick_athletes_h2 = Sélectionnez vos athlètes
index_pick_athletes_1  = Pour chaque épreuve, sélectionnez autant ou aussi peu d’athlètes que vous le souhaitez. Votre budget est limité de manière à ce que vous ne sélectionnez pas que les meilleurs athlètes.
index_pick_athletes_2  = Choisissez un capitaine et il pourra récolter le double de points; choisissez attentivement puisque les shidos compteront aussi pour le double!


index_climb_leaderboard_h2 = Grimpez au classement
index_climb_leaderboard_1  = Comparez les résultats de votre équipe à ceux des autres équipes et voyez si vous pouvez obtenir la première place... ou au moins être mieux classé que les autres membres de votre dojo.
index_climb_leaderboard_2  = Choisir tôt et mettre à jour votre équipe après le tirage au sort sont de bons moyens de gagner quelques points supplémentaires.


index_fine_tune_h2 = Ajustez vos choix
index_fine_tune_1  = Ne vous reposez pas sur vos lauriers—En choisissant tôt, vous pourriez faire une bonne affaire, mais comme les inscriptions changent, surtout après le tirage au sort, vos choix pourraient nécessiter des ajustements.
index_fine_tune_2  = Avec chaque épreuve du circuit mondial de l’IJF, vous ne manquerez pas de choses à faire!


index_follow_action_h2 = Suivez l’action
index_follow_action_1  = Pour les épreuves du circuit mondial de l'IJF, le score de votre équipe sera mis à jour régulièrement pendant la compétition, afin que vous puissiez suivre les progrès de votre équipe.
index_follow_action_2  = Pour les épreuves de moindre importance, la mise à jour se fait le lundi, quand les résultats sont disponibles.


index_discuss_h2 = Discutez pour le plaisir
index_discuss_1  = En plus de parler des résultats de votre équipe au dojo, discutez avec nous et avec d'autres personnes sur { $discordLink }, { $facebookLink } et { $twitterLink }.


index_free_h2 = Jouez gratuitement
index_free_1  = Ce site est complètement gratuit, sans publicité. Nous sommes entièrement soutenus par nos parrains sur { $patreonLink }.
index_free_2  = Veuillez donc envisager de verser une petite contribution chaque mois pour aider à maintenir le site en ligne et à l'améliorer.


## Events


events_7am_local_time = (7am heure locale)
events_closes_in = Ferme dans
events_opens_in = Ouvre dans
events_remaining = restant


## Forgot Password


forgot_password = Mot de passe oublié
forgot_password_sent_1 = Un email de réinitialisation du mot de passe est en route
forgot_password_sent_2 = Surveillez votre boîte de réception pour un email de la part de no-reply@fantasy-judo.com.
forgot_password_sent_3 = Le email contiendra un lien vers une page où vous pourrez déterminer un nouveau mot de passe.


## Log-in


login_alert_reset_success = Le mot de passe a été réinitialisé avec succès, veuillez vous connecter.
login_alert_reset_failure = Email ou mot de passe incorrect, veuillez essayer de nouveau.


## Picks


picks_back    = Retour
picks_save    = Sauvegarder
picks_alert   = Choisissez un capitaine et il pourra récolter le double de points; choisissez attentivement puisque les shidos compteront aussi pour le double!
picks_caption = *Les statistiques couvrent les épreuves de Janvier 2019 à aujourd’hui.
picks_cost    = Coût
picks_wins    = Victoires


## Privacy


privacy_our_contact_details     = Nos détails de contact
privacy_data_protection_officer = Officier de protection des données
privacy_what_info               = Quel type d’information nous détenons
privacy_what_info_2             = Nous recueillons et traitons actuellement les informations suivantes :
privacy_what_info_3             = Adresse email
privacy_how_and_why             = Comment nous obtenons l’information et pourquoi nous l’avons
privacy_how_and_why_2           = Les informations personnelles que nous traitons est fournie par vous pour une des raisons suivantes :
privacy_how_and_why_3           = Création du compte
privacy_how_and_why_4           = Contact email (via “opt-in” sur la page de vos paramètres, le consentement peut être révoqué en tout temps dans cette page aussi)


privacy_gdpr_header             = En vertu du règlement général sur la protection des données (RGPD), les bases légales sur lesquelles nous nous appuyons pour traiter ces informations sont :
privacy_gdpr_1                  = Votre consentement. Vous pouvez retirer votre consentement en tout temps.
privacy_gdpr_2                  = Nous avons des obligations contractuelles.
privacy_gdpr_3                  = Nous avons des obligations légales.
privacy_gdpr_4                  = Nous avons un intérêt vital.
privacy_gdpr_5                  = Nous en avons besoin pour mener à bien une tâche publique.
privacy_gdpr_6                  = Nous avons un intérêt légitime.


privacy_what_info_header        = Que faisons nous de l’information que nous avons
privacy_what_info_1             = Nous utilisons les informations que vous nous avez fournies afin d’avoir une méthode pour contacter les utilisateurs afin de les informer des nouvelles et des mises à jour du site.
privacy_what_info_2             = Nous ne pouvons partager ces informations avec aucune organisation tierce, à l'exception des fournisseurs de services de messagerie lors de l'envoi de courriers électroniques.


privacy_how_we_store_header     = Comment conservons nous vos informations
privacy_how_we_store_1          = Vos informations sont sécuritairement entreposées à Londres, Royaume-Uni.
privacy_how_we_store_2          = Nous conservons votre adresse électronique tant que votre compte existe. Nous détruisons ensuite vos informations en supprimant votre adresse électronique de notre base de données.


privacy_your_rights             = Vos droits de protection de vos données
privacy_your_rights_2           = En vertu de la loi de protection des données, vos droits incluent :
privacy_your_rights_3           = Votre droit d’accès - Vous avez le droit de nous demander des copies de vos informations personnelles.
privacy_your_rights_4           = Votre droit à la rectification - Vous avez le droit de nous demander de rectifier des informations que vous jugez erronées. Vous avez aussi le droit de nous demander de compléter des informations que vous jugez incomplète.
privacy_your_rights_5           = Votre droit à la suppression - Vous avez le droit de nous demander de supprimer vos informations personnelles dans certaines circonstances.
privacy_your_rights_6           = Votre droit à la restriction de traitement - Vous avez le droit de nous demander de restreindre le traitement de vos informations dans certaines circonstances.
privacy_your_rights_7           = Votre droit de vous objecter au traitement - Vous avez le droit de vous objecter au traitement de vos données personnelles dans certaines circonstances.
privacy_your_rights_8           = Votre droit à la portabilité des données - Vous avez le droit de demander que nous transférions les informations que vous nous avez fournies à une autre organisation, ou à vous-même, dans certaines circonstances.




privacy_your_rights_9           = Vous n'êtes pas tenu de payer des frais pour exercer vos droits. Si vous faites une demande, nous avons un mois pour vous répondre.


privacy_complain                = Comment déposer une plainte
privacy_complain_2              = Vous pouvez également déposer une plainte auprès de l'ICO si vous n'êtes pas satisfait de la manière dont nous avons utilisé vos données.




## Results


results_most_picked = Les plus sélectionnés
results_top_scoring = Meilleurs scores


results_team_capt         = Capt.
results_team_collapse_all = Réduire tout
results_team_opponent     = Adversaire
results_team_round        = Ronde
results_team_total        = Total
results_team_versus_me    = Contre moi
results_team_win          = Victoire


results_compare_category  = Catégorie


## Info


info_about_intro_1  = Cette page présente un peu le site web Fantasy Judo, les outils que nous utilisons et les personnes qui l'ont construit.




info_about_who      = Qui
info_about_who_2    = Fantasy Judo a été créé par
info_about_who_3    = et
info_about_who_4    = Collègues de travail, James est l'esprit technique responsable d'une grande partie de l'excellence technique que le site s'efforce d'atteindre. Lance est entraîneur de judo et membre des équipes informatiques de l'Union européenne de judo et de la Fédération internationale de judo, ce qui lui confère des connaissances spécifiques au judo.


info_about_what     = Quoi
info_about_what_2   = Le site a été écrit dans
info_about_what_3   =  avec une base de données
info_about_what_4   =  Les données et les photos proviennent directement des systèmes de données de
info_about_what_5   =  Les icônes proviennent de
info_about_what_6   =  et les images utilisées pour la création des emblèmes de


info_about_where    = Où
info_about_where_2  = Le site est hébergé sur
info_about_where_3  = , le code est entreposé sur
info_about_where_4  = . Le domaine est enregistré et géré dans
info_about_where_5  =  et James et Lance sont basés dans le sud de l’Angleterre au Royaume-Uni.


info_about_when    = Quand
info_about_when_2  = Le développement a débuté en décembre 2018, et la première version a été testée par un groupe d’utilisateur le 1er janvier 2019. La première épreuve était le Grand Prix de Tel Aviv, le 24 janvier 2019, quand le système de résultats a été utilisé sur le site pour la première fois.


info_about_why     = Pourquoi
info_about_why_2   = Au sein de la communauté de judo, au moins 3 versions d’un système de Fantasy Judo ont existées, mais n’ont pas abouties. La dernière version était active en 2017 et la première était opérée par Mathias Fischer sur
info_about_why_3   =  Il y a eu plusieurs discussions par les experts techniques du judo à propos de l’idée d’un projet de Fantasy Judo au cours des années, mais il n’arrivait jamais en tête de liste des priorités. En décembre 2018, James et Lance ont parlé de leur idée autour de quelques bières après le travail et ont décidé que ce serait un projet amusant sur lequel travailler et se sont mis au travail. James avait de l’expérience avec Go pour le Web suite à son fructueux site
info_about_why_4   = donc Go a été choisi comme langage pour le site. James travaillait sur de vastes pans du code pendant que Lance apportait ses connaissances spécifiques au judo.
