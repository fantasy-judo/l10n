## Global

ja = 日本語

become_patron                 = このゲームを支援する
email                         = メールアドレス
events                        = イベント
info                          = 情報
log-in                        = ログイン
password                      = パスワード
results                       = 結果

## Index

index_hero_text     = 他の柔道ファンと対戦する
index_hero_email    = メールアドレス
index_hero_password = パスワード
index_hero_sign_up  = ユーザー登録

index_intro_text = それぞれのイベントに対して柔道の選手を選び、自分だけの最強のチームを作ろう！
柔道選手の世界ランキングのリストから限られた予算を使って慎重に選びましょう。ランキング上位の選手はランキングが低い選手よりもコストが高いです。

index_create_team_h2 = チームを作る
index_create_team_1  = あなたのチームの名前はいつでも使用可能な名前に変更できます。名前を変更してもこれまでの戦績は保存されます。
index_create_team_2  = あなたのエンブレムはチームのバッジです。色や形を自由にカスタマイズして自分だけのオリジナルエンブレムを作ろう。

index_pick_athletes_h2 = 選手を選ぶ
index_pick_athletes_1  = それぞれのイベントに対して好きな数だけ選手を選びましょう。予算は限られているので強い選手だけを大量にえらぶことはできません。
index_pick_athletes_2  = 自分のチームのキャプテンを選びましょう。キャプテンは他の選手と比べて２倍のポイントを獲得しますが、指導を受けた時の減点も２倍です！

index_climb_leaderboard_h2 = リーダーボードを上る
index_climb_leaderboard_1  = 他のチームと自分のチームの戦績を比べましょう。目指すは１位！…が難しい場合は自分の道場にいる他の人たちに勝つことを目標としましょう。
index_climb_leaderboard_2  = 事前に選び引き分けの後にアップデートすることで追加のポイントを手に入れましょう。

index_fine_tune_h2 = 自分の選択を変更する

index_follow_action_h2 = 最近の試合の結果を見る
index_follow_action_1  = IJF　ワールドツアーのイベントでは、あなたのチームの戦績がすぐわかるようにスコアが大会中に頻繁にアップデートされます。
index_follow_action_2  = 小規模のイベントは結果が出る月曜日にアップデートされます。

index_discuss_h2 = 試合について議論する

index_free_h2 = 無料でプレーする
index_free_1  = このサイトは完全無料であり、広告も一切ありません。私たちは全て{ $patreonLink }によって支援されています。
index_free_2  = なので、毎月ほんの少しのお金を払うことで私たちはとても助かります。そのお金はこのサイトの寿命をのばし、サイトをより改善することに使われます。
